//#####################################################################
// Mass-spring deformable model
// Dartmouth COSC 89.18/189.02: Computational Methods for Physical Systems, Assignment starter code
// Contact: Bo Zhu (bo.zhu@dartmouth.edu)
//#####################################################################

#ifndef __SoftBodyMassSpring_h__
#define __SoftBodyMassSpring_h__
#include "Common.h"
#include "Particles.h"

template<int d> class SoftBodyMassSpring
{
	using VectorD = Vector<real, d>; using VectorDi = Vector<int, d>; using MatrixD = Matrix<real, d>;
public:
	////Spring parameters
	Particles<d> particles;
	Array<Vector2i> springs;
	Array<real> rest_length;
	Array<real> ks;
	Array<real> kd;

	////Boundary nodes
	Hashtable<int, VectorD> boundary_nodes;

	////Body force
	VectorD g = VectorD::Unit(1)*(real)-1.;

	enum class TimeIntegration { ExplicitEuler, ImplicitEuler } time_integration = TimeIntegration::ImplicitEuler;

	////Implicit time integration
	SparseMatrixT K;
	VectorX u, b;

	virtual void Initialize()
	{
		////Initialize default spring parameters for standard tests
		real ks_0 = (real)1, kd_0 = (real)1;
		switch (time_integration) {
		case TimeIntegration::ExplicitEuler: {
			ks_0 = (real)5e2;
			kd_0 = (real)1e1;
		}break;
		case TimeIntegration::ImplicitEuler: {
			ks_0 = (real)1e5;
			kd_0 = (real)1e1;
		}break;
		}

		////Allocate arrays for springs and parameters
		rest_length.resize(springs.size());
		for (int i = 0; i < (int)springs.size(); i++) {
			const Vector2i& s = springs[i];
			rest_length[i] = (particles.X(s[0]) - particles.X(s[1])).norm();
		}
		ks.resize(springs.size(), ks_0);
		kd.resize(springs.size(), kd_0);

		////Allocate sparse matrix if using implicit time integration 
		////This function needs to be called for only once since the mesh doesn't change during the simulation)
		if (time_integration == TimeIntegration::ImplicitEuler)
			Initialize_Implicit_K_And_b();
	}

	virtual void Advance(const real dt)
	{
		switch (time_integration) {
		case TimeIntegration::ExplicitEuler:
			Advance_Explicit_Euler(dt); break;
		case TimeIntegration::ImplicitEuler:
			Advance_Implicit_Euler(dt); break;
		}
	}

	////Set boundary nodes
	void Set_Boundary_Node(const int p, const VectorD v = VectorD::Zero()) { boundary_nodes[p] = v; }

	bool Is_Boundary_Node(const int p) { return boundary_nodes.find(p) != boundary_nodes.end(); }

	void Enforce_Boundary_Conditions()
	{
		for (auto p : boundary_nodes) {
			int idx = p.first;					////get boundary particle index
			const VectorD& v = p.second;			////get boundary particle velocity
			particles.V(idx) = v;					////set boundary particle velocity
			particles.F(idx) = VectorD::Zero();
		}	////clear boundary particle force
	}

	//////////////////////////////////////////////////////////////////////////
	////P1 TASK: explicit Euler integration and spring force calculation

	//////////////////////////////////////////////////////////////////////////
	////YOUR IMPLEMENTATION (P1 TASK): explicit Euler time integration 
	void Advance_Explicit_Euler(const real dt)
	{
		Particle_Force_Accumulation();

		////Step 1) update particle velocity; Step 2) update particle position
		/* Your implementation start */

		for (int i = 0; i < particles.Size(); i++) {
			particles.V(i) += dt * (1 / particles.M(i)) * particles.F(i);
			particles.X(i) += dt * particles.V(i);
		}

		/* Your implementation end */
	}

	//////////////////////////////////////////////////////////////////////////
	////YOUR IMPLEMENTATION (P1 TASK): compute spring force f_ij=f_s+f_d 
	VectorD Spring_Force_Calculation(const int idx)
	{
		/* Your implementation start */

		// Some pre calculations
		int i = springs[idx][0];
		int j = springs[idx][1];


		//VectorD l_i_j = (particles.X(j) - particles.X(i)).norm();
		VectorD n_i_j = (particles.X(j) - particles.X(i)) / (particles.X(j) - particles.X(i)).norm();
		VectorD v_i_j = particles.V(j) - particles.V(i);


		VectorD f_s = ks[idx] * n_i_j * ((particles.X(j) - particles.X(i)).norm() - rest_length[idx]);
		VectorD f_d = kd[idx] * v_i_j.dot(n_i_j) * n_i_j;

		VectorD f_i = f_s + f_d;
		return f_i;

		/* Your implementation end */

		//return VectorD::Zero();	////replace this line with your implementation
	}

	VectorD Vector_Cwise_Abs(VectorD vec) {
		VectorD abs_vec = vec;
		for (int i = 0; i < vec.size(); i++) {
			abs_vec[i] = abs(vec[i]);
		}
		return abs_vec;
	}

	VectorD Vector_Cwise_Div(VectorD dividend, VectorD divisor) {
		VectorD quotient = dividend;
		for (int i = 0; i < dividend.size(); i++) {
			quotient[i] = dividend[i] / divisor[i];
		}
		return quotient;
	}

	//////////////////////////////////////////////////////////////////////////
	////YOUR IMPLEMENTATION (P1 TASK): accumulate spring forces to particles
	void Particle_Force_Accumulation()
	{
		////Clear forces on particles
		for (int i = 0; i < particles.Size(); i++) { particles.F(i) = VectorD::Zero(); }

		////Accumulate body forces
		for (int i = 0; i < particles.Size(); i++) {
			particles.F(i) += particles.M(i)*g;
		}

		////Accumulate spring forces
		/* Your implementation start */

		for (int s = 0; s < springs.size(); s++) {
			VectorD f_i = Spring_Force_Calculation(s);
			particles.F(springs[s][0]) += f_i;	// i
			particles.F(springs[s][1]) -= f_i;	// j
		}

		/* Your implementation end */

		////Enforce boundary conditions
		Enforce_Boundary_Conditions();
	}

	//////////////////////////////////////////////////////////////////////////
	////YOUR IMPLEMENTATION (P2 TASK): 
	////Construct K, step 1: initialize the matrix structure 
	void Initialize_Implicit_K_And_b()
	{
		int n = d * particles.Size();
		K.resize(n, n); u.resize(n); u.fill((real)0); b.resize(n); b.fill((real)0);
		Array<TripletT> elements;
		for (int s = 0; s < (int)springs.size(); s++) {
			int i = springs[s][0]; int j = springs[s][1];
			Add_Block_Triplet_Helper(i, i, elements);
			Add_Block_Triplet_Helper(i, j, elements);
			Add_Block_Triplet_Helper(j, i, elements);
			Add_Block_Triplet_Helper(j, j, elements);
		}
		K.setFromTriplets(elements.begin(), elements.end());
		K.makeCompressed();
	}

	//////////////////////////////////////////////////////////////////////////
	////YOUR IMPLEMENTATION (P2 TASK): 
	////Construct K, step 2: fill nonzero elements in K
	void Update_Implicit_K_And_b(const real dt)
	{
		////Clear K and b
		K.setZero();
		b.fill((real)0);

		/* Your implementation start */
		int n = (int)d*particles.Size();

		SparseMatrixT Big_Ks;
		Big_Ks.resize(n, n);
		Big_Ks.setZero(); 

		SparseMatrixT Big_Kd;
		Big_Kd.resize(n, n);
		Big_Kd.setZero();

		// Spring force computation
		for (int s = 0; s < (int)springs.size(); s++) {
			MatrixD Ks_block;
			Ks_block.resize(d, d);
			Ks_block.setZero();
			Compute_Ks_Block(s, Ks_block);
			Add_Block_Helper(Big_Ks, springs[s][0], springs[s][1], Ks_block);
		}

		// Damping force computation
		for (int s = 0; s < (int)springs.size(); s++) {
			MatrixD Kd_block;
			Kd_block.resize(d, d);
			Kd_block.setZero();
			Compute_Kd_Block(s, Kd_block);
			Add_Block_Helper(Big_Kd, springs[s][0], springs[s][1], Kd_block);
		}


		SparseMatrixT M;
		M.resize(n, n);
		M.setZero();
		for (int i = 0; i < particles.Size(); i++) {
			for (int j = 0; j < d; j++) {
				M.coeffRef(i*d + j, i*d + j) = particles.M(i);
			}
		}

		VectorX v_n;
		v_n.resize(n);
		v_n.fill((real)0);
		for (int i = 0; i < particles.Size(); i++) {
			for (int j = 0; j < d; j++) {
				v_n[i*d + j] = particles.V(i)[j];
			}
		}

		VectorX f_n;
		f_n.resize(n);
		f_n.fill((real)0);
		for (int i = 0; i < particles.Size(); i++) {
			for (int j = 0; j < d; j++) {
				f_n[i*d + j] = particles.F(i)[j];
			}
		}

		K = M - dt * Big_Kd - pow(dt, 2)* Big_Ks;
		b = M * v_n + dt * f_n - dt * Big_Kd*v_n;
		 
		// impose boundary conditions 		for (auto p : boundary_nodes) {			particles.V(p.first) = p.second;		}

		/* Your implementation end */
	}

	//////////////////////////////////////////////////////////////////////////
	////P2 TASK: Implicit Euler time integration

	//////////////////////////////////////////////////////////////////////////
	////YOUR IMPLEMENTATION (P2 TASK): 
	////Construct K, step 2.1: compute spring force derivative
	void Compute_Ks_Block(const int s, MatrixD& Ks_block)
	{
		/* Your implementation start */
		// The particles
		int i = springs[s][0];
		int j = springs[s][1];
		real l_0 = rest_length[s];
		VectorD diff_x = particles.X(j) - particles.X(i);

		// Identity
		MatrixD I;
		I.resize(d, d);
		I.setZero();
		for (int c = 0; c < d; c++) {
			I(c, c) = 1;
		}

		Ks_block += ks[s] * ((((l_0 / diff_x.norm()) - 1) * I) - (l_0 / (std::pow(diff_x.norm(), 3))) * (diff_x) * (diff_x.transpose()));
		/* Your implementation end */
	}

	//////////////////////////////////////////////////////////////////////////
	////YOUR IMPLEMENTATION (P2 TASK): 
	////Construct K, step 2.2: compute damping force derivative
	void Compute_Kd_Block(const int s, MatrixD& Kd_block)
	{
		/* Your implementation start */
		int i = springs[s][0];
		int j = springs[s][1];
		
		VectorD n_i_j = (particles.X(j) - particles.X(i)) / (particles.X(j) - particles.X(i)).norm();
		VectorD n_i_i = VectorD::Zero();//0 * -1 * n_i_j;
		VectorD n_j_i = n_i_j;
		VectorD n_j_j = VectorD::Zero();//0 * n_i_i;

		Kd_block(0, 1) += kd[s] * n_i_j.dot(n_i_j);
		Kd_block(0, 0) += kd[s] * n_i_i.dot(n_i_i);
		Kd_block(1, 0) += kd[s] * n_j_i.dot(n_j_i);
		Kd_block(1, 1) += kd[s] * n_j_j.dot(n_j_j);
		/* Your implementation end */
	}

	////Implicit Euler time integration
	void Advance_Implicit_Euler(const real dt)
	{
		Particle_Force_Accumulation();
		Update_Implicit_K_And_b(dt);

		for (int i = 0; i < particles.Size(); i++) {
			for (int j = 0; j < d; j++)u[i*d + j] = particles.V(i)[j];
		}	////set initial guess to be the velocity from the last time step

		SparseSolver::CG(K, u, b);	////solve Ku=b using Conjugate Gradient

		for (int i = 0; i < particles.Size(); i++) {
			VectorD v; for (int j = 0; j < d; j++)v[j] = u[i*d + j];
			particles.V(i) = v;
			particles.X(i) += particles.V(i)*dt;
		}
	}

protected:
	////Add block nonzeros to sparse matrix elements (for initialization)
	void Add_Block_Triplet_Helper(const int i, const int j, Array<TripletT>& elements)
	{
		for (int ii = 0; ii < d; ii++)for (int jj = 0; jj < d; jj++)elements.push_back(TripletT(i*d + ii, j*d + jj, (real)0));
	}

	////Add block Ks to K_ij
	void Add_Block_Helper(SparseMatrixT& K, const int i, const int j, const MatrixD& Ks)
	{
		SparseFunc::Add_Block<d, MatrixD>(K, i, i, Ks);
		SparseFunc::Add_Block<d, MatrixD>(K, j, j, Ks);
		if (!Is_Boundary_Node(i) && !Is_Boundary_Node(j)) {
			SparseFunc::Add_Block<d, MatrixD>(K, i, j, -Ks);
			SparseFunc::Add_Block<d, MatrixD>(K, j, i, -Ks);
		}
	}

	////Set block values on a vector
	void Set_Block(VectorX& b, const int i, const VectorD& bi)
	{
		for (int ii = 0; ii < d; ii++)b[i*d + ii] = bi[ii];
	}

	////Add block values to a vector
	void Add_Block(VectorX& b, const int i, const VectorD& bi)
	{
		for (int ii = 0; ii < d; ii++)b[i*d + ii] += bi[ii];
	}
};

#endif