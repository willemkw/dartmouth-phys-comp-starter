//#####################################################################
// Particle Sand (DEM)
// Dartmouth COSC 89.18/189.02: Computational Methods for Physical Systems, Assignment starter code
// Contact: Bo Zhu (bo.zhu@dartmouth.edu)
//#####################################################################

#ifndef __ParticleSand_h__
#define __ParticleSand_h__
#include "Common.h"
#include "Particles.h"
#include "ImplicitGeometry.h"

template<int d> class ParticleSand
{using VectorD=Vector<real,d>;using VectorDi=Vector<int,d>;using MatrixD=Matrix<real,d>;
public:
	Particles<d> particles;
	real ks=(real)2e2;		////stiffness for the collision force
	real kd=(real).5e1;		////damping for the collision force
	VectorD g=VectorD::Unit(1)*(real)-1.;	////gravity

	////list of implicit geometries describing the environment, by default it has one element, a circle with its normals pointing inward (Bowl)
	Array<ImplicitGeometry<d>* > env_objects;	
	Array<Vector2i> particle_particle_collision_pairs;
	Array<Vector2i> particle_environment_collision_pairs;

	Array<VectorD> my_object_vertices={{-1.,5.},{1.,5.}};	////this array stores the positions of the contour of your object for visualization

	virtual void Advance(const real dt)
	{
		////Clear forces on particles
		for(int i=0;i<particles.Size();i++){
			particles.F(i)=VectorD::Zero();}

		////Accumulate body forces
		for(int i=0;i<particles.Size();i++){
			particles.F(i)+=particles.M(i)*g;}

		Particle_Environment_Collision_Detection();
		Particle_Environment_Collision_Response();
		Particle_Particle_Collision_Detection();
		Particle_Particle_Collision_Response();
		Particle_My_Object_Collision_Detection_And_Response();

		for(int i=0;i<particles.Size();i++){
			particles.V(i)+=particles.F(i)/particles.M(i)*dt;
			particles.X(i)+=particles.V(i)*dt;}
	}

	//////////////////////////////////////////////////////////////////////////
	////YOUR IMPLEMENTATION (P1 TASK): detect collision between particles and env_objects (implicit surface) and record the detected collisions in particle_environment_collision_pairs
	////env_objects is a list of implicit geometries, by default there is only one geometry (the bowl) in the list
	////Each element in particle_environment_collision_pairs is a Vector2i, with the first index for the particle and the second index for the env_objects
	virtual void Particle_Environment_Collision_Detection()
	{
		particle_environment_collision_pairs.clear();
		/* Your implementation start */
		for (int i = 0; i < particles.Size(); i++) {
			for (int j = 0; j < env_objects.size(); j++) {
				// real phi=env_objects[j]->Phi(particles.X(i));

				real phi = env_objects[j]->Phi(particles.X(i));
				if (phi - particles.R(i) < 0)  {
					// Collision detected
					Vector2i z;
					z << i, j;
					particle_environment_collision_pairs.push_back(z);
					// std::cout << "Collision\n" << std::endl;
				}
			}
		}



		/* Your implementation end */
	}
		
	//////////////////////////////////////////////////////////////////////////
	////YOUR IMPLEMENTATION (P1 TASK): compute the penalty-based collision force for the particles that are colliding with the env_objects
	////The collision response force consists of a spring force and a damping force
	virtual void Particle_Environment_Collision_Response()
	{
		for(int pair_idx=0;pair_idx<particle_environment_collision_pairs.size();pair_idx++){
			int i=particle_environment_collision_pairs[pair_idx][0];	////particle index
			int j=particle_environment_collision_pairs[pair_idx][1];	////env_objects index
			VectorD collision_force=VectorD::Zero();

			/* Your implementation start */
			// Required values
			ImplicitGeometry<d>* env_obj = env_objects[j];
			real r_i = particles.R(i);
			VectorD c_i = particles.X(i);
			VectorD v_i = particles.V(i);
			//VectorD v_i = VectorD::Zero();
			VectorD l_0 = VectorD::Zero();

			real phi = env_obj->Phi(c_i);
			VectorD neg_norm = -1 * env_obj->Normal(c_i);

			

			// Spring force
			VectorD f_s = ks * (phi - r_i) * neg_norm;

			// Damping force
			VectorD f_d = kd * ((l_0 - v_i).dot(neg_norm)) * neg_norm;

			collision_force += f_s + f_d;

			/* Your implementation end */
			
			particles.F(i)+=collision_force;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	////YOUR IMPLEMENTATION (P1 TASK): find all the pairs of particles that are colliding each other and record the detected pairs in particle_particle_collision_pairs
	////Each element in particle_particle_collision_pairs is a Vector2i specifying the indices of the two colliding particles
	virtual void Particle_Particle_Collision_Detection()
	{
		particle_particle_collision_pairs.clear();
		/* Your implementation start */
		for (int i = 0; i < particles.Size(); i++) {
			for (int j = 0; j < particles.Size(); j++){
				if (j != i){
					VectorD c_i = particles.X(i); 
					VectorD c_j = particles.X(j); 
					real r_i = particles.R(i); 
					real r_j = particles.R(j); 
					if ((c_i - c_j).norm() - (r_i + r_j) < 0){
						Vector2i z;
						z << i, j;
						particle_particle_collision_pairs.push_back(z);
					} 
				}
			}
		}

		/* Your implementation end */
	}

	//////////////////////////////////////////////////////////////////////////
	////YOUR IMPLEMENTATION (P1 TASK): compute penalty-based collision forces for pairs of colliding particles in particle_particle_collision_pairs and add the forces to particle.F 
	////The collision response force for each pair consists of a spring force and a damping force
	virtual void Particle_Particle_Collision_Response()
	{
		for(int pair_idx=0;pair_idx<particle_particle_collision_pairs.size();pair_idx++){
			int i=particle_particle_collision_pairs[pair_idx][0];	////the first particle index in the pair
			int j=particle_particle_collision_pairs[pair_idx][1];	////the second particle index in the pair
			VectorD collision_force=VectorD::Zero();

			/* Your implementation start */
			real r_i  = particles.R(i); 
			real r_j = particles.R(j); 
			VectorD x_i = particles.X(i); 
			VectorD x_j  = particles.X(j); 
			VectorD v_i = particles.V(i); 
			VectorD v_j = particles.V(j); 
			real l_0 = r_i + r_j;

			VectorD f_s = ks * ((x_j - x_i).norm() - l_0) * (x_j - x_i) / (x_j - x_i).norm(); 
			VectorD f_d = kd * (v_j - v_i).dot((x_j - x_i) / (x_j - x_i).norm()) * (x_j - x_i) / (x_j - x_i).norm() ; 

			collision_force += f_s + f_d;
			particles.F(i)+=collision_force;
			particles.F(j)-=collision_force; 
			/* Your implementation end */
		}

	}

	//////////////////////////////////////////////////////////////////////////
	////YOUR IMPLEMENTATION (P1 TASK): implement your the collision detection algorithm and calculate the response forces for your own object
	////YOUR IMPLEMENTATION (P1 TASK): visualize your object by filling up the contour vertex array
	////Note: the visualization does not have to be the same as the way you represent the geometry, 
	////E.g., you may use an implicit function to calculate the geometry and the collision force, but still use the vertices to draw it
	virtual void Particle_My_Object_Collision_Detection_And_Response()
	{
		////if you want to visualize your object, fill the array of my_object_shape with its contour vertices, the commented code provides an example
		//my_object_vertices.clear();
		//my_object_vertices.push_back(Vector2(-2.,5.));
		//my_object_vertices.push_back(Vector2(2.,5.));

		for(int i=0;i<particles.Size();i++){
			VectorD collision_force=VectorD::Zero();
			
			/* Your implementation start */
			/* Your implementation end */
			
			particles.F(i)+=collision_force;
		}	
	}
};

#endif
