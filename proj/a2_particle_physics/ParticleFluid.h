//#####################################################################
// Particle Fluid (SPH)
// Dartmouth COSC 89.18/189.02: Computational Methods for Physical Systems, Assignment starter code
// Contact: Bo Zhu (bo.zhu@dartmouth.edu)
//#####################################################################
#ifndef __ParticleFluid_h__
#define __ParticleFluid_h__
#include "Common.h"
#include "Particles.h"
#include "ImplicitGeometry.h"

//////////////////////////////////////////////////////////////////////////
////Kernel function
template<int d> class Kernel
{
	using VectorD = Vector<real, d>;
public:
	////precomputed coefs;
	real h;
	real coef_Wspiky;
	real coef_dWspiky;
	real coef_Wvis;
	real coef_d2Wvis;
	real pi = 3.1415927;

	void Precompute_Coefs(real _h)
	{
		h = _h;
		coef_Wspiky = 15.0 / (pi*pow(h, 6));
		coef_dWspiky = -45.0 / (pi*pow(h, 6));
		coef_Wvis = 2 * pi*pow(h, 3);
		coef_d2Wvis = 45.0 / (pi*pow(h, 6));
	}

	////Kernel Spiky
	real Wspiky(const VectorD& xji)
	{
		real r = xji.norm();
		if (r >= 0 && r <= h) { return 15.0 / (pi*pow(h, 6))*pow(h - r, 3); }
		else { return 0; }
	}
	VectorD gradientWspiky(const VectorD& v) {
		real r = v.norm();
		if (r <= h && r > 0) { return -45.0 / (pi*pow(h, 6))*pow(h - r, 2)*v / r; }
		else { return VectorD::Zero(); }
	}

	////Kernel viscosity
	real Wvis(const VectorD& xji) {
		real r = xji.norm();
		if (r >= 0 && r <= h) { return 15.0 / (2 * pi*pow(h, 3))*((-pow(r, 3) / (2 * pow(h, 3)) + r * r / (h*h) + h / (2 * r) - 1)); }
		else { return 0; }
	}
	real laplacianWvis(const VectorD& v) {
		real r = v.norm();
		if (r <= h && r > 0) { return 45.0 / (pi*pow(h, 6))*(h - r); }
		else { return 0; }
	}
};

//////////////////////////////////////////////////////////////////////////
////Spatial hashing
template<int d> class SpatialHashing
{
	using VectorD = Vector<real, d>; using VectorDi = Vector<int, d>;
public:
	real dx = 1.;	////grid cell size
	Hashtable<VectorDi, Array<int> > voxels;

	void Update_Voxels(const Array<VectorD>& points)
	{
		Clear_Voxels(); for (int i = 0; i < (int)points.size(); i++)Add_Point(i, points[i]);
	}

	void Clear_Voxels() { voxels.clear(); }

	bool Add_Point(const int point_idx, const VectorD& point_pos)
	{
		VectorDi cell = Cell_Coord(point_pos);
		auto iter = voxels.find(cell);
		if (iter == voxels.end())iter = voxels.insert(std::make_pair(cell, Array<int>())).first;
		Array<int>& bucket = iter->second;
		bucket.push_back(point_idx);
		return true;
	}

	//////////////////////////////////////////////////////////////////////////
	////YOUR IMPLEMENTATION (P2 TASK): find all the neighboring particles within the "kernel_radius" around "pos" and record their indices in "nbs", the position of the particles are given in "points"
	////You need to traverse all the 3^d neighboring cells in the background grid around the cell occupied by "pos", and then check the distance between each particle in each neighboring cell and the given "pos"
	////Use the helper function Cell_Coord to get the cell coordinates for a given "pos"
	////Use the helper function Nb_R to get the cell coordinates of the ith neighboring cell around the cell "coord"
	bool Find_Nbs(const VectorD& pos, const Array<VectorD>& points, const real kernel_radius,/*returned result*/Array<int>& nbs) const
	{
		/* Your implementation start */
		VectorDi cell_coord = Cell_Coord(pos); // gets the cell coordinates in the background grid for a given position 
		int num_neighbor_cells = std::pow(3, d); // number of neighboring cells (3^d)
		for (int i = 0; i < num_neighbor_cells; i++) { // iterate over neighboring cells 
			VectorDi neighbor_cell_coord = Nb_R(cell_coord, i); // get the cell coordinates of the ith neighboring cell around cell_coord

			// voxels.find was the problem. Find takes key type. We were putting in value type.
			if (voxels.find(neighbor_cell_coord) != voxels.end()) {
				const Array<int>& neighbor_particle_indices = voxels.at(neighbor_cell_coord); // get all the particle indices within neighbor_cell_coord
				for (int p = 0; p < neighbor_particle_indices.size(); p++) { // iterate over particle indices within neighbor_cell_coord
					VectorD neighbor = points[neighbor_particle_indices[p]]; // get the position of that particle index 
					if ((neighbor - pos).norm() < kernel_radius) { // check if the distance is less than kernel radius 
						nbs.push_back(neighbor_particle_indices[p]); // add to nbs 
					}
				}
			}
		}
		/* Your implementation end */
		return nbs.size() > 0;
	}

protected:	////Helper functions
	VectorDi Cell_Coord(const VectorD& pos) const
	{
		VectorD coord_with_frac = (pos) / dx; return coord_with_frac.template cast<int>();
	}
	Vector2i Nb_R(const Vector2i& coord, const int index) const
	{
		assert(index >= 0 && index < 9); int i = index / 3; int j = index % 3; return coord + Vector2i(-1 + i, -1 + j);
	}
	Vector3i Nb_R(const Vector3i& coord, const int index) const
	{
		assert(index >= 0 && index < 27); int i = index / 9; int m = index % 9; int j = m / 3; int k = m % 3; return coord + Vector3i(-1 + i, -1 + j, -1 + k);
	}
};

//////////////////////////////////////////////////////////////////////////
////Particle fluid simulator
template<int d> class ParticleFluid
{
	using VectorD = Vector<real, d>;
public:
	Particles<d> particles;
	Array<Array<int> > neighbors;
	SpatialHashing<d> spatial_hashing;
	Kernel<d> kernel;

	real kernel_radius = (real).8;			////kernel radius
	real pressure_density_coef = (real)1e1;	////pressure-density-relation coefficient, used in Update_Pressure()
	real density_0 = (real)10.;				////rest density, used in Update_Pressure()
	real viscosity_coef = (real)1e1;			////viscosity coefficient, used in Update_Viscosity_Force()
	real kd = (real)1e2;						////stiffness for environmental collision response
	VectorD g = VectorD::Unit(1)*(real)-1.;	////gravity

	Array<VectorD> Delta_P;
	Array<real> lambda;						//// scaling factor for particles
	Array<real> C;							//// Constraints
	Array<real> p;							//// Position constraints
	Array<real> s_corr;						//// 
	real e = 1;								//// Relaxation constant. TODO: Determine what value it should take.
	real solver_iters = 2;					//// Number of iterations for solver

	////Environment objects
	Array<ImplicitGeometry<d>* > env_objects;

	virtual void Initialize()
	{
		kernel.Precompute_Coefs(kernel_radius);
		lambda.resize(particles.Size(), (real)0);
		C.resize(particles.Size(), (real)0);
		p.resize(particles.Size(), (real)0);
		s_corr.resize(particles.Size(), (real)0);
	}

	virtual void Update_Neighbors()
	{
		spatial_hashing.Clear_Voxels();
		spatial_hashing.Update_Voxels(particles.XRef());

		neighbors.resize(particles.Size());
		for (int i = 0; i < particles.Size(); i++) {
			Array<int> nbs;
			spatial_hashing.Find_Nbs(particles.X(i), particles.XRef(), kernel_radius, nbs);
			neighbors[i] = nbs;
		}
	}

	virtual void Advance(const real dt)
	{
		for (int i = 0; i < particles.Size(); i++) {
			particles.F(i) = VectorD::Zero();
		}

		Update_Neighbors();
		Update_Density();
		Update_Pressure();
		Update_Pressure_Force();
		Update_Viscosity_Force();
		Update_Boundary_Collision_Force();


		for (int iter = 0; i < solver_iters; i++) {
			Update_Lambda();
			Update_Delta_P();
		}

		/*
		for each iteration
			Update_Lambda
			Update_Delta_P
			Update_Inter_Pos
		*/

		for (int i = 0; i < particles.Size(); i++) {
			particles.V(i) += particles.F(i) / particles.D(i)*dt;
			particles.X_inter(i) = particles.X(i) + particles.V(i)*dt;	// Predict position
			//particles.X(i) += particles.V(i)*dt;
		}
	}


	//////////////////////////////////////////////////////////////////////////
	////Update the density (particles.D(i)) of each particle based on the kernel function (Wspiky)
	void Update_Density()
	{
		for (int i = 0; i < particles.Size(); i++) {
			real sum_neighbors = 0; // for each particle, compute the sum across all neighbors of m_j * W_ji
			for (int j = 0; j < neighbors[i].size(); j++) { // iterate through all of the neighbors 
				int neighbor_idx = neighbors[i][j];

				VectorD neighbor = particles.X(neighbor_idx);
				VectorD particle = particles.X(i);
				VectorD xji = particle - neighbor;
				real m_j = particles.M(neighbor_idx);
				real W_ji = kernel.Wspiky(xji);
				real sigma = m_j * W_ji;

				sum_neighbors += sigma;
			}
			particles.D(i) = sum_neighbors;
		}
	}

	//////////////////////////////////////////////////////////////////////////
	////Update the pressure (particles.P(i)) of each particle based on its current density (particles.D(i)) and the rest density (density_0)
	void Update_Pressure()
	{
		real k = pressure_density_coef;
		real rho_0 = density_0;
		for (int i = 0; i < particles.Size(); i++) {
			particles.P(i) = k * (particles.D(i) - rho_0);
		}
	}

	void Update_Lambda()
	{
		// TODO: Update lambda. See step 4 in google doc & equation 9 in paper
		for (int i = 0; i < particles.Size(); i++) {
			real lambda_i;

			// define density constraint on particle i
			real constraint = particles.D(i) / density_0 - 1;
			real grad_constraint_sum = 0; // initialize sum of squared gradient norms of neighbors

			// calculate gradient of constraint function on particle i with respect to neighbor j
			for (int k = 0; k < neighbors[i].size(); k++) {
				VectorD grad_constraint = VectorD::Zero();

				// case where k is the current particle
				if (particles.X(i) == particles.X(neighbors[i][k])) {
					for (int j = 0; j < neighbors[i].size(); j++) {
						Vector p_ij = particles.P(i) - particles.P(neighbors[i][j]));
						VectorD W = kernel.gradientWspiky(p_ij);
						grad_constraint += W;
					}
				}
				// case where k is a neighboring particle
				else {
					VectorD p_ik = particles.P(i) - particles.P(neighbors[i][k]));
					VectorD W = kernel.gradientWspiky(p_ik);
					grad_constraint = -1 * W;
				}

				grad_constraint /= density_0;
				grad_constraint_sum += pow(grad_constraint.norm(), 2);
			}

			// lambda for particle i
			lambda_i = -1 * constraint / grad_constraint_sum;

			lambda[i] = lambda_i;
		}
	}


	void Update_Delta_P() {
		// TODO: Update p_i. See step 5 in google doc & equation 12 in paper
		// Collision detection & response
		// xi*+= pi
		
		for (int i = 0; i < particles.Size(); i++) {
			VectorD delta_p = VectorD::Zero();
			VectorD sum;

			for (int j = 0; j < neighbors[i].size(); j++) {
				VectorD p_ij = particles.P(i) - particles.P(neighbors[i][j]);
				sum += (lambda[i] + lambda[neighbors[i][j]]) * gradientWspiky(p_ij);
			}
			delta_p = sum / density_0;
			Delta_P[i] = delta_p;

			Collision_Detect_And_Response()
		}
	}

	//////////////////////////////////////////////////////////////////////////
	////YOUR IMPLEMENTATION (P2 TASK): compute the pressure force for each particle based on its current pressure (particles.P(i)) and the kernel function gradient (gradientWspiky), and then add the force to particles.F(i)
	void Update_Pressure_Force()
	{
		/* Your implementation start */
		for (int i = 0; i < particles.Size(); i++) {
			VectorD sum_neighbors = VectorD::Zero();
			for (int j = 0; j < neighbors[i].size(); j++) {
				int neighbor_idx = neighbors[i][j];
				VectorD neighbor = particles.X(neighbor_idx);
				VectorD particle = particles.X(i);
				VectorD v = particle - neighbor;
				sum_neighbors += (particles.P(i) + particles.P(neighbor_idx)) / 2 * particles.M(j) / particles.D(j) * kernel.gradientWspiky(v);
			}
			particles.F(i) += -1 * sum_neighbors;
		}
		/* Your implementation end */
	}

	//////////////////////////////////////////////////////////////////////////
	////YOUR IMPLEMENTATION (P2 TASK): compute the viscosity force for each particle based on its current velocity difference (particles.V(j)-particles.V(i)) and the kernel function Laplacian (laplacianWvis), and then add the force to particles.F(i)
	void Update_Viscosity_Force()
	{
		/* Your implementation start */
		for (int i = 0; i < particles.Size(); i++) {
			VectorD sum_neighbors = VectorD::Zero();
			for (int j = 0; j < neighbors[i].size(); j++) {
				int neighbor_idx = neighbors[i][j];
				VectorD neighbor = particles.X(neighbor_idx);
				VectorD particle = particles.X(i);
				VectorD v = particle - neighbor;
				sum_neighbors += (particles.V(neighbor_idx) - particles.V(i)) * particles.M(j) / particles.D(j) * kernel.laplacianWvis(v);

			}
			particles.F(i) += viscosity_coef * sum_neighbors;
		}
		/* Your implementation end */
	}

	void Update_Body_Force()
	{
		for (int i = 0; i < particles.Size(); i++) {
			particles.F(i) += particles.D(i)*g;
		}
	}

	void Update_Boundary_Collision_Force()
	{
		for (int i = 0; i < particles.Size(); i++) {
			
		}
	}

	void Collision_Detect_And_Response() {
		for (int j = 0; j < env_objects.size(); j++) {
			real phi = env_objects[j]->Phi(particles.X(i));
			if (phi < particles.R(i)) {
				VectorD normal = env_objects[j]->Normal(particles.X(i));
				particles.F(i) += normal * kd*(particles.R(i) - phi)*particles.D(i);
			}
		}
	}
};

#endif
